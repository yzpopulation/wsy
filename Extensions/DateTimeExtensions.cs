﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace wsy.Extensions
{
   public static class DateTimeExtensions
    {

        public static DateTime ToDateTime(this string s)
        {
            IFormatProvider culture = new CultureInfo("zh-CN", true);
            string[] expectedFormats =
            {
                "yyyy-MM-dd HH:mm:ss",
                "yyyy-MM-dd HH:mm:ss.fff",
                "yyyy-M-d HH:mm:ss",
                "yyyy-M-d HH:mm:ss.fff",
                "yyyy年M月d日 HH时mm分ss秒",
                "yyyy年MM月dd日 HH时mm分ss秒",
                "yyyyMMddHHmmss",
                "yyyyMMdd",
                "yyyyMd",
            };
            return DateTime.ParseExact(s, expectedFormats, culture, DateTimeStyles.AllowInnerWhite);
        }

    }
}

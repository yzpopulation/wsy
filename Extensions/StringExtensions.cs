﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using DevLib.ExtensionMethods;

namespace wsy.Extensions
{
    public static class StringExtensions
    {
        public static int ToInt(this string s)
        {
            return int.Parse(s);
        }

        public static float ToFloat(this string s)
        {
            return float.Parse(s);
        }

        public static double ToDouble(this string s)
        {
            return double.Parse(s);
        }

        public static void co()
        {
            Console.WriteLine("yes".YesNoToBool());
        }

    }

}


